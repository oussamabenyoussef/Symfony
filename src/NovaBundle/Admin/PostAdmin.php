<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/08/16
 * Time: 13:14
 */

namespace NovaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use NovaBundle\Entity\Post;


class PostAdmin extends AbstractAdmin {


    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', 'text')

              ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper

            ->add('title')


        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }



    public function toString($object)
    {
        return $object instanceof Post
            ? $object->getTitle()
            : ' Post Manager '; // shown in the breadcrumb on the create view
    }


}