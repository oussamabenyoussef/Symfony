<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 27/08/16
 * Time: 13:14
 */

namespace NovaBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use NovaBundle\Entity\Image;
use NovaBundle\Form\ImageType;

class AdvertAdmin extends AbstractAdmin {


    protected function configureFormFields(FormMapper $formMapper)
    {


        $formMapper
         ->add('date', 'datetime')
         ->add('title', 'text')
         ->add('author', 'text')
         ->add('content', 'textarea')
         ->add('categories')
         ->add('image')
         ->add('image', new ImageType(), array(
                    'data_class' => 'NovaBundle\Entity\Image')
            )
      ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
          ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
    }


    public function toString($object)
    {
        return $object instanceof Image
            ? $object->getAlt()
            : 'Advert Manager'; // shown in the breadcrumb on the create view
    }
}