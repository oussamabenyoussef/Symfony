<?php

namespace NovaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use NovaBundle\Form\ImageType;
use NovaBundle\Form\CategoryType;

class AdvertType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', 'datetime')
            ->add('title')
            ->add('author')
            ->add('content')
            ->add('categories')

            ->add('image', new ImageType(), array(
                    'data_class' => 'NovaBundle\Entity\Image')
            )



        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'NovaBundle\Entity\Advert'
        ));
    }
}
