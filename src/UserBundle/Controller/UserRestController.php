<?php

namespace UserBundle\Controller;


use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\RestBundle\Controller\FOSRestController;

class UserRestController extends Controller
{

    public function getUserAction($username){
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneByUsername($username);
        if(!is_object($user)){
            throw $this->createNotFoundException();
        }
        return $user;
    }


}